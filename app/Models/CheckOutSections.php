<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckOutSections extends Model
{
    protected $table = 'check_out_section';
}
