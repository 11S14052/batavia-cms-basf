<?php

namespace App\Http\Controllers;

use DB;
use App\Models\MasterCheckIn;
use App\Models\CheckInSections;
use Illuminate\Http\Request;

class CheckinStatementController extends Controller
{
    public function statementlist(){
        $section = CheckInSections::all();
        $statement = MasterCheckIn::orderBy('section_id', 'asc')
        ->get();
        if ($statement) {
            return view('check_in_statement.statement_list')->with('datastatement', $statement)->with('datasection', $section);
        }
    }
    public function saveCreate(){
        $statement = new MasterCheckIn;
        $statement->desc_check_in = request('desc');
        $statement->section_id = request('section');
        $statement->save();
        return redirect('/checkin-statement')->with('message', 'Berhasil Ditambahkan!');
    }

    public function edit(Request $request, $statement_id){
        $statement = MasterCheckIn::find($statement_id);
        if ($statement){
            return view('check_in_statement.edit_statement')->with('statement', $statement);
        }
    }
    public function delete(Request $request, $statement_id){
        $statement = MasterCheckIn::find($statement_id);
        $statement->delete();
        return redirect('/checkin-statement')->with('message', 'Berhasil Dihapus!');
    }

    public function saveUpdate(){
        $statement = MasterCheckIn::find(request('id'));
        $statement->desc_check_in = request('desc');
        $statement->section_id = request('section');
        $statement->save();
        return redirect('/checkin-statement')->with('message', 'Berhasil Ditambahkan!');
    }
}
