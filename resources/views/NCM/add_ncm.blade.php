@extends('master')

@php
    $datatype = DB::select('select * from transporter_type');
@endphp

@section('content')
    <form action="/tambahncmprocess" method="POST" enctype="multipart/form-data">
        @csrf
        <div>
            <label for="ncm">NCM</label>
            <input type="text" id="ncm" name="ncm" required>
        </div>
        <input class="form-control" type="hidden" name="id" value="{{$datareport->id}}">
        <div>
            <button class="btn btn-primary" type="submit">Tambah</button>
        </div>
    </form>
@endsection