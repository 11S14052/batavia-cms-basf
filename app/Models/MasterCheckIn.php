<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCheckIn extends Model
{
    protected $table = 'master_check_in';
    public $primaryKey = 'id';
}
