<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function loginprocess(Request $request){
        session()->regenerate();
        $nik = request('nik');
        $password = request('password');
        $login = Users::where('id_employee', $nik)->first();
        if (Hash::check($password, $login['password'])) {
            $request->session()->put('nama', $login['name']);
            $request->session()->put('privilege', $login['id_privilege']);
            $request->session()->put('posisi', $login['position']);
            $request->session()->put('image', $login['image']);
            $request->session()->put('nik', $login['id_employee']);
            return redirect('dashboard');
        }else {
            return view('login.login_app')->with('message', 'NIK atau Password salah!');
        }
    }
}
