<?php

namespace App\Http\Controllers;

use DB;
use App\Models\MasterCheckOut;
use App\Models\CheckOutSections;
use Illuminate\Http\Request;

class CheckoutStatementController extends Controller
{
    public function statementlist(){
        $section = CheckOutSections::all();
        $statement = MasterCheckOut::orderBy('section_id', 'asc')
        ->get();
        if ($statement) {
            return view('check_out_statement.statement_list')->with('datastatement', $statement)->with('datasection', $section);
        }
    }
    public function create(){
        $statement = new MasterCheckOut;
        $statement->desc_check_out = request('desc');
        $statement->section_id = request('section');
        $statement->save();
        return redirect('/checkout-statement')->with('message', 'Berhasil Ditambahkan!');
    }

    public function edit(Request $request, $statement_id){
        $statement = MasterCheckOut::find($statement_id);
        if ($statement){
            return view('check_out_statement.edit_statement')->with('statement', $statement);
        }
    }
    public function delete(Request $request, $statement_id){
        $statement = MasterCheckOut::find($statement_id);
        $statement->delete();
        return redirect('/checkout-statement')->with('message', 'Berhasil Dihapus!');
    }

    public function saveUpdate(){
        $statement = MasterCheckOut::find(request('id'));
        $statement->desc_check_out = request('desc');
        $statement->section_id = request('section');
        $statement->save();
        return redirect('/checkout-statement')->with('message', 'Berhasil Ditambahkan!');
    }
}
