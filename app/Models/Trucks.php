<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trucks extends Model
{
    protected $table = 'trucks';
    public $primaryKey = 'id';
}
