@extends('master')

@section('content')
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Number</th>
                <th>Previllage</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($dataprivilege as $privilege)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $privilege->name }}</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i> Actions</button>
                        <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                            <div class="dropdown-menu" role="menu">
                            <!-- <a class="dropdown-item" >Setting Role</a> -->
                            <a class="dropdown-item" onclick="edit({{$privilege->id}})" >Edit</a>
                            <a class="dropdown-item" onclick="deletePrivilege({{$privilege->id}})">Delete</a>
                        </button>
                    </div>
                </td>
            </tr>
            @php
                $i = $i + 1;
            @endphp
            @endforeach
        </tbody>
    </table>
    <a href="add-group">
        <button class="btn btn-primary">Add New Group</button>
    </a>
@endsection

@section('js')
<script>
function edit(id){
window.location.href = "edit-group/" +id;
}
function deletePrivilege(id){
    var r = confirm("Yakin Hapus?")
    if(r== true){
        window.location.href = "hapus_privilege/" +id;
    }
}
</script>    

@endsection