@extends('master')

@section('content')
    <div class="card-header">
        <h6 class="card-title">EDIT GROUP</h6>
    </div>
    <form action="/ubahprivilegeprocess" method="POST" enctype="multipart/form-data">
        @csrf
        <p></p>
        <p></p>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-2">
                        <label for="privilage" class="font-weight-bold">Role Name</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" id="privilage" name="privilege" value="{{$privilege->name}}">
                    </div>
                </div>
            </div>
        </div>
        <p></p>
        <p></p>
        <div>
            <label for="">User Management</label>
            <input type="checkbox" name="view_user" value="1" 
            @if ($privilege->view_user != null)
                checked
            @endif>View User
            <input type="checkbox" name="add_user" value="1"  
            @if ($privilege->add_user != null)
                checked
            @endif>Add User
            <input type="checkbox" name="edit_user" value="1" 
            @if ($privilege->edit_user != null)
                checked
            @endif>Edit User
            <input type="checkbox" name="delete_user" value="1" 
            @if ($privilege->delete_user != null)
                checked
            @endif>Delete User
        </div>
        <div>
            <label for="">Role Management</label>
            <input type="checkbox" name="view_role" value="1" 
            @if ($privilege->view_role != null)
                checked
            @endif>View Role
            <input type="checkbox" name="add_role" value="1" 
            @if ($privilege->add_role != null)
                checked
            @endif>Add Role
            <input type="checkbox" name="setting_role" value="1" 
            @if ($privilege->setting_role != null)
                checked
            @endif>Setting Role
            <input type="checkbox" name="delete_role" value="1" 
            @if ($privilege->delete_role != null)
                checked
            @endif>Delete Role
        </div>
        <div>
            <label for="">Truck Report</label>
            <input type="checkbox" name="view_report" value="1" 
            @if ($privilege->view_report != null)
                checked
            @endif>View Report
            <input type="checkbox" name="detail_report" value="1" 
            @if ($privilege->detail_report != null)
                checked
            @endif>Report Detail
            <input type="checkbox" name="add_ncm" value="1" 
            @if ($privilege->add_ncm != null)
                checked
            @endif>Add NCM
            <input type="checkbox" name="edit_ncm" value="1" 
            @if ($privilege->edit_ncm != null)
                checked
            @endif>Edit NCM
            <input type="checkbox" name="approve_report" value="1" 
            @if ($privilege->approve_report != null)
                checked
            @endif>Approve
            <input type="checkbox" name="cancel_approve" value="1" 
            @if ($privilege->cancel_approve != null)
                checked
            @endif>Cancel Report
        </div>
        <div>
            <label for="">Trash Report</label>
            <input type="checkbox" name="view_trash" value="1" 
            @if ($privilege->view_trash != null)
                checked
            @endif>View Trash
            <input type="checkbox" name="restore_trash" value="1" 
            @if ($privilege->restore_trash != null)
                checked
            @endif>Restore Trash
            <input type="checkbox" name="remove_permanent" value="1" 
            @if ($privilege->remove_permanent != null)
                checked
            @endif>Remove Permanent
        </div>
        <input type="hidden" name="id" value="{{$privilege->id}}">
        <div>
            <button type="submit">Ubah</button>
        </div>
    </form>
@endsection