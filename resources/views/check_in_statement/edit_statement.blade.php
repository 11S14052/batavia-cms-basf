@extends('master')

@php
    $datasection = DB::select('select * from check_in_section');
@endphp

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="card-title">CHECKIN STATEMENT - Edit</h5>
    </div>
</div>
<hr/>
<p></p>
<form action="/save-update" method="POST" enctype="multipart/form-data">
    @csrf
    <div>
        <div class="row">
            <div class="col-md-1">
                <label for="section">Section</label>
            </div>
            <div class="col-md-3">
                <select name="section" id="section">
                    @foreach ($datasection as $section)
                        @if ($statement->section_id === $section->id)
                            <option value="{{$section->id}}" selected>{{ $section->description }}</option>
                        @else
                            <option value="{{$section->id}}">{{ $section->description }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-1">
                <label for="desc">Description</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="desc" name="desc" required value="{{$statement->desc_check_in}}">
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="{{$statement->id}}">
    <hr/>
    <div>
        <button class="btn btn-primary" type="submit">Update</button>
    </div>
</form>
@endsection