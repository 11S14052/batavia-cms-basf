<?php

namespace App\Http\Controllers;

use DB;
use App\report;
use Illuminate\Http\Request;
use App\Models\GeneralInfoForm;

class TrashController extends Controller
{
    public function daftartrash(){
        $reportingTrucks= GeneralInfoForm::join('drivers','drivers.id','general_info_form.driver_id')
            ->select('general_info_form.created_at as datetime_in_or_out', 'general_info_form.carrier_name', 'general_info_form.vehicle_number'
                    ,'general_info_form.tank_number', 'drivers.name', 'general_info_form.vehicle_type', 'general_info_form.DO_number', 'general_info_form.id', 'general_info_form.isOpen', 'general_info_form.isApprove')
            ->whereNotNull('general_info_form.isApprove')
            ->where('general_info_form.isDeleted', 1)
            ->get();
        if ($reportingTrucks) {
            return view('trash_report.trash_list')->with('reportingTrucks', $reportingTrucks);
        }
    }

    public function restore($reportId){
        $updateGeneralInform = GeneralInfoForm::where('id', $reportId)->first();
        $updateGeneralInform->isDeleted= 0;
        $updateGeneralInform->save();
        return redirect('trash')->with('message', 'Berhasil restore!');
    }

    public function removePermanent($reportId){
        $updateGeneralInform = GeneralInfoForm::where('id', $reportId)->first();
        $updateGeneralInform->delete();
        return redirect('trash')->with('message', 'Berhasil di hapus permanent!');
    }
}
