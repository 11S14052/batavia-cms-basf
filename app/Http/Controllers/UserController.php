<?php

namespace App\Http\Controllers;
use DB;
use App\Models\Users;
use App\Models\Privileges;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function daftaruser(){
        $user = Users::join('privileges','privileges.id','users.id_privilege')
                ->select('privileges.name as privilege_name', 'users.id as users_id', 'users.name', 'users.email', 'users.is_active')
                ->get();
        if ($user) {
            return view('user.user_list')->with('datauser', $user);
        }
    }

    public function editUser(Request $request, $userId){
        $user = Users::join('privileges','privileges.id','users.id_privilege')
                ->select('privileges.name as privilege_name', 'users.phone', 'users.id_employee', 'users.id as users_id', 'users.name', 'users.email', 'users.is_active')
                ->where('users.id', $userId)
                ->first();
        $getAllPrivileges = Privileges::get();
        if ($user){
            return view('user.ubah_user', ['user'=>$user, 'dataPrivileges'=>$getAllPrivileges]);
        }
    }

     public function saveUpdateUserManagement(){
        $usersId = request('users_id');
        $user = Users::where('id',$usersId)->first();
        $user->name = request('name');
        $user->email = request('email');
        $user->is_active = request('is_active');
        $user->id_privilege = request('id_privilege');
        $user->phone = request('phone');
        $user->save();
        return redirect('user/management')->with('message', 'Berhasil Diubah!');
    }

    public function tambahuser(){
        $this->validateimage();
        $user = new user;
        $user->NIK = request('nik');
        $user->nama = request('nama');
        $user->email = request('email');
        $user->image = 'profile/default.png';
        $user->posisi = request('posisi');
        $user->jenis_kelamin = request('jenis_kelamin');
        $user->active = request('status');
        $user->nomor_telepon = request('telepon');
        $user->tanggallahir = request('tanggallahir');
        $user->password = md5(request('password'));
        $user->save();
        return redirect('user')->with('message', 'Berhasil Ditambahkan!');
    }

    public function storeImage($image){
        if (request()->has('image')){
            $image->update([
                'image'=> request()->image->store('profile', 'public'),
            ]);
            $image = Image::make(public_path('storage/' . $image->image))->resize(800, 600);
            $image->save();
        } 
    }

    public function validateimage(){
        if (request()->hasFile('image')){
            request()->validate([
                'image'=> 'file|image|max:7000',
            ]);
        }
    }
    public function hapus_user(Request $request, $usersId){
        // if (session('role')=='Admin') {
            $user = Users::where('id',$usersId)->first();
            $user->delete();
            return redirect('/user')->with('message', 'Berhasil Dihapus!');
        // } else {
            // return redirect('/restaurant_admin')->with('message', 'Anda Bukan Admin!');
        // }
    }

    

    
}
