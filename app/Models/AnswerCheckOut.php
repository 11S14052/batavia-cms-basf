<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerCheckOut extends Model
{
    protected $table = 'answer_check_out';
}
