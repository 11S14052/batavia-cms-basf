@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="box-title">TRANSPORTER TYPE - List </h5>
    </div>
</div>
<hr/>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
            <tr>
                <th>No.</th>
                <th>Transporter Type</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($datatransporter as $transporter)
        <tr>   
            <td>{{ $i }}</td>
            <td>{{ $transporter->type }}</td>
            <td>{{ $transporter->description }}</td>
            <td>
                <button class="btn btn-primary btn-block"onclick="edit({{$transporter->id}})"><i class="fa fa-edit"></i>Edit</button>
                <button class="btn btn-primary btn-block"onclick="confirmdelete({{$transporter->id}})">Delete</button>
            </td>
            @php
                $i = $i + 1;
            @endphp
        </tr>
        @endforeach
    </tbody>
</table>
        
    <a href="/transporter-type/create">
        <button class="btn btn-primary">Add New Transporter Type</button>
    </a>
@endsection

@section('js')
<script>
function edit(id){
window.location.href = "transporter-type/edit/" +id;
}
function confirmdelete(id){
    var r = confirm("Yakin Hapus?")
    if(r== true){
        window.location.href = "transporter-type/delete/" +id;
    }
}
</script>    

@endsection