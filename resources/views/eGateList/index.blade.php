@extends('master')

@section('content')
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
            <tr>
                <th>No</th>
                <th>Jam Masuk/Keluar</th>
                <th>Nama Angkutan / Pelanggan / Supplier</th>
                <th>No.Kendaraan</th>
                <th>Nama Driver</th>
                <th>Jenis Kendaraan</th>
                <th>Code</th>
                <th>Status</th>
                <th>Kesimpulan</th>
                <th style="width: 150px">Action</th>
            </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($reportingTrucks as $truck)
        <tr>   
            <td>{{ $i }}</td>
            <td>{{ $truck->datetime_in_or_out }}</td>
            <td>{{ $truck->carrier_name}}</td>
            <td>{{ $truck->vehicle_number}}</td>
            <td>{{ $truck->name}}</td>
            <td>{{ $truck->vehicle_type}}</td>
            <td>{{ $truck->DO_number}}</td>
            <td>
                @if($truck->isOpen==1) 
                    <center>
                        <p style="color:green">Open</p>
                    </center>
                @elseif($truck->isOpen==0) 
                    <center>
                        <p style="color:red">Close</p>
                    </center>
                @endif
            </td>
            <td>
                @if($truck->isApprove==1) 
                    <center>
                        <span style="color:white" class="badge bg-success">Diterima</span>
                    </center>
                @elseif($truck->isApprove==0) 
                    <center>
                        <span style="color:white" class="badge bg-danger">Ditolak</span>
                    </center>
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i> Actions</button>
                    <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" onclick="edit({{$truck->id}})">Detail</a>
                        <!-- <a class="dropdown-item" href="#">Add NCM</a> -->
                        @if($truck->isApprove==1) 
                            <a class="dropdown-item" onClick="rejectModal({{$truck->id}})">Cancel Approve</a>
                        @elseif($truck->isApprove==0) 
                            <a class="dropdown-item" onClick="approveModal({{$truck->id}})">Cancel Reject</a>
                        @endif
                    </button>
                </div>
            </td>
            @php
                $i = $i + 1;
            @endphp
        </tr>
        @endforeach
    </tbody>
    <div class="modal fade" id="modalApproval" tabindex="-1" role="dialog" aria-labelledby="approveLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="approveLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="approve-bodyku">
          </div>
          <div class="modal-footer" id="approve-footerq">
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="rejectLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="rejectLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="reject-bodyku">
          </div>
          <div class="modal-footer" id="reject-footerq">
          </div>
        </div>
      </div>
    </div>
</table>
@endsection

@section('js')
<script>

function edit(id){
    window.location.href = "reporting/detail/" +id;
}

function confirmdelete(id){
    var r = confirm("Sure, deleted this user?")
    if(r== true){
        window.location.href = "hapus_user/" +id;
    }
}

function approveModal(id){
    localStorage.setItem("idGeneralInform", id);
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Are you sure you want to approve this truck ?</label></div></form>';
    var title = 'Approve Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="cancelReject(id)" type="button" class="btn btn-primary">Yes, cancel reject</button>';
    setModalBoxApprove(title,content,footer,size);
    $('#modalApproval').modal('show');
 }
 function rejectModal(id){
    localStorage.setItem("idGeneralInform", id);
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Are you sure you want to cancel this approve ?</label></div> <div class="row"><div class="col-md-1"></div><div class="col-md-2"><label></div></form>';
    var title = 'Reject Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="cancelApprove()" type="button" class="btn btn-primary">Yes, reject</button>';
    setModalBoxReject(title,content,footer,size);
    $('#modalReject').modal('show');
 }

 function setModalBoxApprove(title,content,footer,$size){
    document.getElementById('approve-bodyku').innerHTML=content;
    document.getElementById('approveLabel').innerText=title;

    document.getElementById('approve-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#approveModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#approveModal').attr('class', 'modal fade')
            .attr('aria-labelledby','approveLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#approveModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

  function setModalBoxReject(title,content,footer,$size){
    document.getElementById('reject-bodyku').innerHTML=content;
    document.getElementById('rejectLabel').innerText=title;

    document.getElementById('reject-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#rejectModal').attr('class', 'modal fade')
            .attr('aria-labelledby','rejectLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

function cancelReject(){
    const id = localStorage.getItem("idGeneralInform");
    localStorage.clear();
    window.location.href = '{{ env('APP_URL') }}' + '/reporting/cancel-reject/'+id;
}
// cancel approve
function cancelApprove(){
    const id = localStorage.getItem("idGeneralInform");
    const key = encryptKey('{{ env('URL_KEY') }}');
    const newData = {
                    id:id,
                };
    localStorage.clear();
    // console.log(encryptValue(newData, key))
    const url = '{{ env('APP_URL') }}' + '/reporting/cancel-approve'+"?data="+JSON.stringify(newData);
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send(JSON.stringify(newData));
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            window.location.href = '{{ env('APP_URL') }}' + '/reporting'
        } else{
        }
    };
    
}

</script>    

@endsection