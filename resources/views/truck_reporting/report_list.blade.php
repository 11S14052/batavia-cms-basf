@extends('master')

@section('content')
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
            <tr>
                <th>No</th>
                <th>Check In</th>
                <th>Check Out</th>
                <th>Transporter Name</th>
                <th>Produk</th>
                <th>Nomor Kendaraan</th>
                <th>Nama Pengemudi</th>
                <th>Jenis Kendaraan</th>
                <th>No JO/DO</th>
                <th>Kesimpulan</th>
                <th>Aksi</th>
            </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($datareport as $report)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $report->created_at }}</td>
            <td>{{ $report->checkout_datetime }}</td>
            <td>{{ $report->carrier_name }}</td>
            <td>{{ $report->product_name }}</td>
            <td>{{ $report->vehicle_number }}</td>
            <td>{{ $report->driver_name }}</td>
            <td>{{ $report->vehicle_type }}</td>
            <td>{{ $report->DO_number }}</td>
            <td>{{ $report->summary }}</td>
            <td>
                <button class="btn btn-primary" onclick="detail({{$report->id}})">Detail</button>
                <button class="btn btn-primary" onclick="confirmdelete({{$report->id}})">Hapus</button>
            </td>
        </tr>
        @php
            $i = $i + 1;
        @endphp
        @endforeach
    </tbody>
</table>
@endsection

@section('js')
<script>
function detail(id){
window.location.href = "reportdetail/" +id;
}
function confirmdelete(id){
    var r = confirm("Yakin Hapus?")
    if(r== true){
        window.location.href = "trash_report/" +id;
    }
}
</script>    

@endsection