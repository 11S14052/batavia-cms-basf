@extends('master')

@section('content')
    @if (!empty($massage))
        <script>
            alert("{{$message}}");
        </script>
    @endif
    <h4>Nomor Induk Karyawan : {{ $profile->id_employee }}</h4>
    <div class="row">
        <div class="col-8">
            <form action="/ubahprofile" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="nik" id="nik" value="{{ $profile->id_employee}}">
                <div>
                    <div class="row">
                        <label for="nama" class="col-3">Full Name</label>
                        {{": "}}&nbsp;
                        <input type="text" id="nama" name="nama" value="{{ $profile->name}}" class="border-0 text-capitalize" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="email" class="col-3">E-mail Address</label>
                        {{": "}}&nbsp;
                        <input type="email" id="email" name="email" value="{{ $profile->email}}" class="border-0" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="posisi" class="col-3">Position</label>
                        {{": "}}&nbsp;
                        <input type="text" id="posisi" name="posisi" value="{{ $profile->position}}" class="border-0 text-capitalize" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="telepon" class="col-3">Phone Number</label>
                        {{": "}}&nbsp;
                        <input type="text" id="telepon" name="telepon" value="{{ $profile->phone}}" class="border-0" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="tanggallahir" class="col-3">Date of Birth</label>
                        {{": "}}&nbsp;
                        <input type="date" id="tanggallahir"  name="tanggallahir" value="{{ $profile->birth_date}}" class="border-0" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="privilege" class="col-3">Previllage</label>
                        {{": "}}&nbsp;
                        <input type="text" id="privilege" name="privilege" value="{{ $profile->privilege}}" class="border-0" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="jenis_kelamin" class="col-3">Gender</label>
                        {{": "}}&nbsp;
                        <input type="text" id="jenis_kelamin" name="jenis_kelamin" value="{{ $profile->gender}}" class="border-0" disabled>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="status" class="col-3">Status</label>
                        @if ($profile->is_active != null)
                        {{": "}}&nbsp;
                        <input type="text" id="status" name="status" value="Aktif" class="border-0" disabled>
                        @else
                        {{": "}}&nbsp;
                        <input type="text" id="status" name="status" value="Tidak Aktif" class="border-0" disabled>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <div class="col-4">
            @if ($profile->image =="")
                <img style="border:5px double black;" src="/imageFile/imageProfile/avatar.png" id="profile-img-tag" width="180" height="190"/> 
            @else
                <img style="border:5px double black;" src="/imageFile/imageProfile/{{$profile->image}}" id="profile-img-tag" width="180" height="190"/> 
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-4 p-1 mx-auto">
            <a href="/edit-profile"><button class="btn btn-primary btn-block">Ubah</button></a>
        </div>
        <div class="col-4 p-1 mx-auto">
            <a href="/change-password"><button class="btn btn-primary btn-block">Change Password</button></a>
        </div>
    </div>

@endsection