@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="card-title">CHECKOUT SECTION - List</h5>
    </div>
</div>
<hr/>
<p></p>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Section</th>
            <th>Kelola</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($datasection as $section)
        <tr>
            <td>{{ $section->description }}</td>
            <td>
                <button class="btn btn-primary" onclick="edit({{$section->id}})">Edit</button>
                <button class="btn btn-primary" onclick="confirmdelete({{$section->id}})">Delete</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a href="/checkout-section/create">
    <button class="btn btn-primary">Create Section</button>
</a>
@endsection

@section('js')
<script>
function edit(id){
window.location.href = "checkout-section/edit/" +id;
}
function confirmdelete(id){
    var r = confirm("Yakin Hapus?")
    if(r== true){
        window.location.href = "checkout-section/delete/" +id;
    }
}
</script>    

@endsection