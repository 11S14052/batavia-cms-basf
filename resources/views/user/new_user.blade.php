@extends('master')
@php
    $dataprivilege = DB::select('select * from privileges');
@endphp
@section('content')
    <form action="/tambahuserprocess" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <label for="nik">Nomor Induk Karyawan</label>
                    {{": "}}&nbsp;
                    <input type="text" id="nik" name="nik" required>
                </div>
                <div class="row">
                    <label for="nama">Nama Lengkap</label>
                    {{": "}}&nbsp;
                    <input type="text" id="nama" name="nama" required>
                </div>
                <div class="row">
                    <label for="email">E-mail</label>
                    {{": "}}&nbsp;
                    <input type="email" id="email" name="email" required>
                </div>
                <div>
                    <label for="posisi">Posisi</label>
                    {{": "}}&nbsp;
                    <input type="text" id="posisi" name="posisi" required>
                </div>
                <div>
                    <label for="privilege">Privilege</label>
                    {{": "}}&nbsp;
                    <select name="privilege" id="privilege" required>
                        @foreach ($dataprivilege as $privilege)
                            <option value="{{ $privilege->name }}">{{ $privilege->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <label for="jenis_kelamin">Jenis Kelamin</label>
                    {{": "}}&nbsp;
                    <select name="jenis_kelamin" id="jenis_kelamin" required>
                        <option value="Pria">Pria</option>
                        <option value="Wanita">Wanita</option>
                    </select>
                </div>
                <div>
                    <label for="status">Status</label>
                    {{": "}}&nbsp;
                    <select name="status" id="status" required>
                        <option value="1">Active</option>
                        <option value="">Inactive</option>
                    </select>
                </div>
                <div>
                    <label for="telepon">Nomor Telepon</label>
                    {{": "}}&nbsp;
                    <input type="number" id="telepon" name="telepon" required>
                </div>
                <div>
                    <label for="password">Password</label>
                    {{": "}}&nbsp;
                    <input type="password" id="password" name="password">
                </div>
                <div>
                    <label for="tanggallahir">Tanggal Lahir</label>
                    {{": "}}&nbsp;
                    <input type="date" id="tanggallahir" name="tanggallahir">
                </div>
                <div class="col-4">
                    <label for="image">Foto Profil</label><br/>
                    <input type="file" name="image" id="image">
                    <div>{{ $errors->first('image') }}</div>
                </div>
                <div class="col-5 mt-4 mx-auto">
                    <button class="btn btn-primary btn-block"  type="submit">Tambah</button>
                </div>
    </form>
@endsection