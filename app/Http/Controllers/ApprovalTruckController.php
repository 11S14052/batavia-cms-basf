<?php

namespace App\Http\Controllers;

use App\Models\GeneralInfoForm;
use App\Models\Privileges;
use Illuminate\Support\Facades\Hash;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\MethodeService;

class ApprovalTruckController extends Controller
{
    public function index(){
        $approvalTrucks= GeneralInfoForm::join('drivers','drivers.id','general_info_form.driver_id')
            ->select('general_info_form.created_at as datetime_in_or_out', 'general_info_form.carrier_name', 'general_info_form.vehicle_number'
                    ,'general_info_form.tank_number', 'drivers.name', 'general_info_form.vehicle_type', 'general_info_form.DO_number', 'general_info_form.id', 'general_info_form.isOpen', 'general_info_form.isApprove')
            ->whereNull('general_info_form.isApprove')
            ->where('general_info_form.isDeleted', 0)
            ->get();
        
        return view('approvalTruck.index')->with('approvalTrucks', $approvalTrucks);
    }

    public function detail($truckId){
        $detailApprovalTruck= GeneralInfoForm::join('drivers','drivers.id','general_info_form.driver_id')
            ->join('users','users.id','general_info_form.approver_id_checkin')
            ->select('general_info_form.created_at as datetime_in_or_out', 'general_info_form.carrier_name', 'general_info_form.vehicle_number','general_info_form.tank_number'
                    , 'drivers.name', 'drivers.phone', 'general_info_form.vehicle_type', 'general_info_form.DO_number', 'general_info_form.id', 'general_info_form.isApprove'
                    , 'drivers.license_type', 'drivers.license_number', 'drivers.license_expiry_date', 'general_info_form.vehicle_certificate_expiry_date', 'general_info_form.kir_expiry_date'
                    , 'general_info_form.tank_number', 'general_info_form.DO_number', 'general_info_form.product_name', 'general_info_form.vehicle_type', 'general_info_form.created_at'
                    , 'users.name as operator_name')
            ->where('general_info_form.id', $truckId)
            ->first();
        return view('approvalTruck.detail')->with('detailApprovalTruck', $detailApprovalTruck);
    }

    public function approveTruck($truckId){
        $updateGeneralInform = GeneralInfoForm::where('id', $truckId)->first();
        $updateGeneralInform->isApprove= 1;
        $updateGeneralInform->save();
        return redirect('approval-trucks')->with('message', 'Berhasil Approve Truck!');
    }
    public function cancelReject($truckId){
        $updateGeneralInform = GeneralInfoForm::where('id', $truckId)->first();
        $updateGeneralInform->isApprove= null;
        $updateGeneralInform->isOpen= 1;
        $updateGeneralInform->NCM= null;
        $updateGeneralInform->save();
        return redirect('reporting')->with('message', 'Berhasil Approve Truck!');
    }

    public function cancelApprove(Request $request){
        // $methodService=new MethodeService();
        // $decryptedData=$methodService->decryptCheckKeyAndReturnValue($request->data);
        $jsondata = json_decode($request->data, true);
        $truckId = $jsondata['id'];
        $updateGeneralInform = GeneralInfoForm::where('id', $truckId)->first();
        $updateGeneralInform->isApprove= null;
        $updateGeneralInform->isOpen= 1;
        $updateGeneralInform->save();
        return redirect('reporting')->with('message', 'Berhasil Reject Truck!');
    }

    public function rejectTruck(Request $request){
        // $methodService=new MethodeService();
        // $decryptedData=$methodService->decryptCheckKeyAndReturnValue($request->data);
        $jsondata = json_decode($request->data, true);
        $truckId = $jsondata['id'];
        $ncm_number = $jsondata['ncm_number'];
        $updateGeneralInform = GeneralInfoForm::where('id', $truckId)->first();
        $updateGeneralInform->isApprove= 0;
        $updateGeneralInform->isOpen= 0;
        $updateGeneralInform->NCM= $jsondata['ncm_number'];
        $updateGeneralInform->save();
        return redirect('approval-trucks')->with('message', 'Berhasil Reject Truck!');
    }
}