@extends('master')

@section('content')
    <h1>Dashboard</h1>

            <!-- Donut Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Trend Truck</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4">
                    <canvas id="myPieChart"></canvas>
                  </div>
                 </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      <!-- Page level plugins -->
  <script src="/sbadmin/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="/sbadmin/js/demo/chart-pie-demo.js"></script>

@endsection

