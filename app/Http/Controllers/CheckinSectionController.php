<?php

namespace App\Http\Controllers;

use DB;
use App\Models\CheckInSections;
use Illuminate\Http\Request;

class CheckinSectionController extends Controller
{
    public function sectionlist(){
        $section = CheckInSections::all();
        if ($section) {
            return view('check_in_section.section_list')->with('datasection', $section);
        }
    }
    public function create(){
        $section = new CheckInSections;
        $section->description = request('desc');
        $section->save();
        return redirect('/checkin-section')->with('message', 'Berhasil Ditambahkan!');
    }

    public function edit(Request $request, $section_id){
        $section = CheckInSections::find($section_id);
        if ($section){
            return view('check_in_section.edit_section')->with('section', $section);
        }
    }
    public function delete(Request $request, $section_id){
            $section = CheckInSections::find($section_id);
            $section->delete();
            return redirect('/checkin-section')->with('message', 'Berhasil Dihapus!');
    }

    public function saveUpdate(){
        $section = CheckInSections::find(request('id'));
        $section->description = request('desc');
        $section->save();
        return redirect('/checkin-section')->with('message', 'Berhasil Ditambahkan!');
    }
}
