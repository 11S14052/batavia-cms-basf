@extends('master')

@section('content')
<div class="col-xs-6 col-md-4 col-lg-2 mb-2 pl-0 pr-0">
    <a href="/user/add">
        <button class="btn btn-primary btn-block">Tambah User</button>
    </a>
</div>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>No</th>
            <th>Full Name</th>
            <th>E-mail</th>
            <th>Privillege</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($datauser as $user)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->privilege_name }}</td>
            <td>
                @if ($user->is_active == 1)
                    <center>
                        <span style="color:white" class="badge bg-success">Active</span>
                    </center>
                @else
                    <center>
                        <span style="color:white" class="badge bg-danger">Inactive</span>
                    </center>
                @endif
            </td>

            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i> Actions</button>
                    <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" onclick="edit({{$user->users_id}})" >Edit</a>
                        <a class="dropdown-item" onclick="deleteUser({{$user->users_id}})">Delete</a>
                        <a class="dropdown-item" >Change Password</a>
                    </button>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('js')
<script>
function edit(users_id){
    window.location.href = "edit/" +users_id;
}
function deleteUser(users_id){
    var r = confirm("Sure, deleted this user?")
    if(r== true){
        window.location.href = "hapus_user/" +users_id;
    }
}
</script>    

@endsection