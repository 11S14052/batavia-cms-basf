@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="card-title">CHECKOUT STATEMENT - List</h5>
    </div>
</div>
<hr/>
<p></p>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>No</th>
            <th>Description</th>
            <th>Section</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($datastatement as $statement)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $statement->desc_check_out }}</td>
            <td>
                @foreach ($datasection as $section)
                    @if ($statement->section_id == $section->id)
                        {{ $section->description }}
                    @endif
                @endforeach
            </td>
            <td>
                <button class="btn btn-primary" onclick="edit({{$statement->id}})">Edit</button>
                <button class="btn btn-primary" onclick="confirmdelete({{$statement->id}})">Delete</button>
            </td>
        </tr>
        @php
            $i = $i +1;
        @endphp
        @endforeach
    </tbody>
</table>
<a href="/checkout-statement/create">
    <button class="btn btn-primary">Create Statement</button>
</a>
@endsection

@section('js')
<script>
function edit(id){
window.location.href = "checkout-statement/edit/" +id;
}
function confirmdelete(id){
    var r = confirm("Yakin Hapus?")
    if(r== true){
        window.location.href = "checkout-statement/delete/" +id;
    }
}
</script>    

@endsection