@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="box-title">TRANSPORTER TYPE - Create </h5>
    </div>
</div>
<hr/>
<form action="save-create" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-2">
            <label for="type">Transporter Type</label>
        </div>
        <div class="col-md-5">
            <input type="text" id="type" name="type" required>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-md-2">
            <label for="desc">Description</label>
        </div>
        <div class="col-md-5">
            <input type="text" id="desc" name="desc" required>
        </div>
    </div>
    <hr/>
    <div>
        <button class="btn btn-primary" type="submit">Create</button>
    </div>
</form>
@endsection