<?php

namespace App\Http\Controllers;

use App\Models\GeneralInfoForm;
use App\Models\Privileges;
use App\Models\AnswerCheckIn;
use App\Models\AnswerCheckOut;
use App\Models\Signs;

use Illuminate\Support\Facades\Hash;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EGateListController extends Controller
{
    public function index(){
        $reportingTrucks= GeneralInfoForm::join('drivers','drivers.id','general_info_form.driver_id')
            ->select('general_info_form.created_at as datetime_in_or_out', 'general_info_form.carrier_name', 'general_info_form.vehicle_number'
                    ,'general_info_form.tank_number', 'drivers.name', 'general_info_form.vehicle_type', 'general_info_form.DO_number', 'general_info_form.id', 'general_info_form.isOpen', 'general_info_form.isApprove')
            ->whereNotNull('general_info_form.isApprove')
            ->where('general_info_form.isDeleted', 0)
            ->get();
        return view('eGateList.index')->with('reportingTrucks', $reportingTrucks);
    }

    public function reportDetail($report_id){
        $detailReporting= GeneralInfoForm::join('drivers','drivers.id','general_info_form.driver_id')
            ->join('users','users.id','general_info_form.approver_id_checkin')
            ->select('general_info_form.created_at as checkin_datetime', 'general_info_form.carrier_name', 'general_info_form.vehicle_number','general_info_form.tank_number', 'general_info_form.checkout_datetime'
                    , 'drivers.name', 'drivers.phone', 'general_info_form.vehicle_type', 'general_info_form.DO_number', 'general_info_form.id', 'general_info_form.isApprove'
                    , 'drivers.license_type', 'drivers.license_number', 'drivers.license_expiry_date', 'general_info_form.vehicle_certificate_expiry_date', 'general_info_form.kir_expiry_date'
                    , 'users.name as operator', 'general_info_form.tank_number', 'general_info_form.DO_number', 'general_info_form.product_name', 'general_info_form.vehicle_type')
            ->where('general_info_form.id', $report_id)
            ->first();
        $signsCheckinOperator= Signs::where('id_form', $detailReporting->id)->where('status', '=', 'Checkin')->where('type', '=', 'Approver')->first();
        $signsCheckinDriver= Signs::where('id_form', $detailReporting->id)->where('status', '=', 'Checkin')->where('type', '=', 'Driver')->first();
        $detailReporting->operator_sign_check_in = $signsCheckinOperator->image;
        $detailReporting->operator_sign_check_in_datetime = date("d-m-Y", strtotime($signsCheckinOperator->created_at));
        $detailReporting->driver_sign_check_in = $signsCheckinDriver->image;
        $detailReporting->driver_sign_check_in_in_datetime = date("d-m-Y", strtotime($signsCheckinDriver->created_at));
        // dd($detailReporting);
        // pertanyaan dan jawaban sebelum checkin
        // $getAnswerCheckIns = AnswerCheckIn::join('master_check_in','master_check_in.id','answer_check_in.master_check_in_id')
        //                     ->join('check_in_section','check_in_section.id','master_check_in.section_id')
        //                     ->select('check_in_section.description as title_question', 'master_check_in.desc_check_in as question', 'answer_check_in.approve_check_in', 'answer_check_in.decline_check_in', 'answer_check_in.na_check_in')
        //                     ->where('general_info_id', $report_id)
        //                     ->get();

        // $groupByTitleQuestion = array();
        // foreach ( $getAnswerCheckIns as $value ) {
        //     $groupByTitleQuestion[$value->title_question] = $value;
        // }
        $getAnswerCheckIns = AnswerCheckIn::join('master_check_in','master_check_in.id','answer_check_in.master_check_in_id')
                            ->join('check_in_section','check_in_section.id','master_check_in.section_id')
                            ->select('check_in_section.description as title_question', 'master_check_in.desc_check_in as question', 'answer_check_in.approve_check_in', 'answer_check_in.decline_check_in', 'answer_check_in.na_check_in', 'answer_check_in.annotation_check_in')
                            ->where('general_info_id', $report_id)
                            ->get();

        // if truck checkout
        if($detailReporting->checkout_datetime != null){
            $detailReporting->is_checkout = 1;
            $signsCheckOutOperator= Signs::where('id_form', $detailReporting->id)->where('status', '=', 'Checkout')->where('type', '=', 'Approver')->first();
            $signsCheckOutDriver= Signs::where('id_form', $detailReporting->id)->where('status', '=', 'Checkout')->where('type', '=', 'Driver')->first();
            $detailReporting->operator_sign_check_out = $signsCheckOutOperator->image;
            $detailReporting->operator_sign_check_out_datetime = date("d-m-Y", strtotime($signsCheckOutOperator->created_at));
            $detailReporting->driver_sign_check_out = $signsCheckOutDriver->image;
            $detailReporting->driver_sign_check_out_datetime = date("d-m-Y", strtotime($signsCheckOutDriver->created_at));

            $getAnswerCheckOuts = AnswerCheckOut::join('master_check_out','master_check_out.id','answer_check_out.master_check_out_id')
            ->join('check_out_section','check_out_section.id','master_check_out.section_id')
            ->select('check_out_section.description as title_question', 'master_check_out.desc_check_out as question', 'answer_check_out.approve_check_out', 'answer_check_out.decline_check_out', 'answer_check_out.na_check_out', 'answer_check_out.annotation_check_out')
            ->where('general_info_id', $report_id)
            ->get();
        } else {
            $getAnswerCheckOuts=array();
            $detailReporting->is_checkout = 0;
        }
        return view('eGateList.report_detail',['detailReporting'=> $detailReporting,'groupByTitleQuestion'=>$getAnswerCheckIns, 'getAnswerCheckOuts'=>$getAnswerCheckOuts]);    
    }
}