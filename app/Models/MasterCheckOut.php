<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCheckOut extends Model
{
    protected $table = 'master_check_out';
    public $primaryKey = 'id';
}
