<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckInSections extends Model
{
    protected $table = 'check_in_section';
    public $primaryKey = 'id';
}
