@extends('master')

@section('content')

@php
    $datasection = DB::select('select * from check_out_section');
@endphp
<div>
    <h1> Nomor Dokumen : CGK-{{ $report->id }}</h1>
</div>
<h1>Check Out Detail</h1>
@foreach ($datasection as $section)
    @php
        $dataanswer = DB::table('answer_check_out')
        ->select('answer_check_out.approve_check_out',
                'answer_check_out.decline_check_out',
                'answer_check_out.na_check_out',
                'answer_check_out.annotation_check_out',
                'answer_check_out.section_id',
                'answer_check_out.general_info_id',
                'answer_check_out.master_check_out_id',
                'master_check_out.desc_check_out',
                'master_check_out.id')
        ->leftjoin('master_check_out', 'answer_check_out.master_check_out_id', '=', 'master_check_out.id')
        ->where('answer_check_out.general_info_id', $report->id)
        ->where('answer_check_out.section_id', $section->id)
        ->get();
    @endphp
    <h2>{{ $section->description }}</h2>
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Description</th>
                <th>Approved</th>
                <th>Declined</th>
                <th>N/A</th>
                <th>Information</th>
            </tr>
        </thead>
        @foreach ($dataanswer as $answer)
        <tbody>
            <tr>
                <td>
                    @if ($answer->desc_check_out != null)
                        {{ $answer->desc_check_out }}
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->approve_check_out != null)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->decline_check_out != null)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->na_check_out != null)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->annotation_check_out != null)
                        {{ $answer->annotation_check_out }}
                    @else
                        -
                    @endif
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
@endforeach
<div class="my-1 mx-auto">
    <a href="/trashdetail/{{$report->id}}">
        <button class="btn btn-primary col-12">Back</button>
    </a>
</div>
@endsection