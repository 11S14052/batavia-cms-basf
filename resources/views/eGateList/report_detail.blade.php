@extends('master')

@section('content')
<div class="card direct-chat direct-chat-primary">
    <div class="card-header">
        <div class="row">
            <div class="col-md-9">
                <h6 class="card-title">DETAIL - PEMERIKSAAN KENDARAAN PENGANGKUT MATERIAL DAN BAHAN BERBAHAYA</h6>
            </div>
            <div class="col-md-3">
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-print"></i>  Print</button>
                    <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="#">FORM 1</a>
                        <a class="dropdown-item" href="#">FORM 2</a>
                    </button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i>  Export</button>
                    <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="#">FORM MASUK SITE</a>
                        <a class="dropdown-item" href="#">FORM KELUAR SITE</a>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <h4> <i class="far fa-dot-circle"></i> Pemasok (Supplier)</h4>
    </div>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Tanggal & Jam</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="truck_name" name="truck_name" value="{{$detailReporting->checkin_datetime}}" >
        </div>
        <div class="col-md-2">
            <label>Jenis SIM</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="license_type" name="license_type" value="{{$detailReporting->license_type}}">
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Operator</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="truck_name" name="truck_name" value="{{$detailReporting->operator}}" >
        </div>
        <div class="col-md-2">
            <label>No.SIM</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="license_number" name="license_number" value="{{$detailReporting->license_number}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Nama Angkutan/ Pelanggan/ Supplier</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="carrier_name" name="carrier_name" value="{{$detailReporting->carrier_name}}" >
        </div>
        <div class="col-md-2">
            <label>SIM berlaku sampai</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="license_expiry_date" name="license_expiry_date" value="{{$detailReporting->license_expiry_date}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. Kendaraan</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="vehicle_number" name="vehicle_number" value="{{$detailReporting->vehicle_number}}" >
        </div>
        <div class="col-md-2">
            <label>STNK berlaku sampai</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="vehicle_certificate_expiry_date" name="vehicle_certificate_expiry_date" value="{{$detailReporting->vehicle_certificate_expiry_date}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. Tangki</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="tank_number" name="tank_number" value="{{$detailReporting->tank_number}}" >
        </div>
        <div class="col-md-2">
            <label>KIR berlaku sampai</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="kir_expiry_date" name="kir_expiry_date" value="{{$detailReporting->kir_expiry_date}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. JO/DO</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="DO_number" name="DO_number" value="{{$detailReporting->DO_number}}" >
        </div>
        <div class="col-md-2">
            <label>Jenis Kendaraan</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="vehicle_type" name="vehicle_type" value="{{$detailReporting->vehicle_type}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Nama Driver</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="name" name="name" value="{{$detailReporting->name}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. Telp Driver</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="phone" name="phone" value="{{$detailReporting->phone}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Nama Produk</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="product_name" name="product_name" value="{{$detailReporting->product_name}}" >
        </div>
    </div>
    <p></p>
    <p></p>    
    <p></p>    
    <h4>SEBELUM MASUK SITE</h4>
    <div>
        <table class="table table-bordered"  width="70%" cellspacing="0">
            <thead>
                    <tr>
                        <th>No</th>
                        <th>Aktifitas</th>
                        <th>Ya</th>
                        <th>Tidak</th>
                        <th>N/A</th>
                        <th>Keterangan</th>
                    </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($groupByTitleQuestion as $groupByTitleQuestion)
                <tr>   
                    <td>{{ $i }}</td>
                    <td>{{$groupByTitleQuestion->question}}</td>
                    <td>
                        @if($groupByTitleQuestion->approve_check_in==1) 
                            <center>
                                <input type="radio" checked disabled>
                            </center>
                        @elseif($groupByTitleQuestion->approve_check_in==0) 
                            <center>
                                <input type="radio" disabled>
                            </center>
                        @endif
                    </td>
                    <td>
                        @if($groupByTitleQuestion->decline_check_in==1) 
                            <center>
                                <input type="radio" checked disabled>
                            </center>
                        @elseif($groupByTitleQuestion->decline_check_in==0) 
                            <center>
                                <input type="radio" disabled>
                            </center>
                        @endif
                    </td>
                    <td>
                        @if($groupByTitleQuestion->na_check_in==1) 
                            <center>
                                <input type="radio" checked disabled>
                            </center>
                        @elseif($groupByTitleQuestion->na_check_in==0) 
                            <center>
                                <input type="radio" disabled>
                            </center>
                        @endif
                    </td>
                    <td>{{$groupByTitleQuestion->annotation_check_in}}</td>
                    @php
                        $i = $i + 1;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
        <p></p>
        <p></p>
        <div class="row">
            <div class="col-md-8">
                <p>Kesimpulan</p>
                @if($detailReporting->isApprove==1) 
                    <input type="radio" checked> Diterima
                @elseif($detailReporting->isApprove==0) 
                    <input type="radio" > Ditolak
                @endif
            </div>
            <div class="col-md-4">
                <table style="float: left;" class="tblsign table-bordered table-striped stacktable large-only" width="70%" cellspacing="0" cellpadding="2" border="1">
                    <tbody>
                        <tr>
                            <td>Diperiksa Oleh</td>
                            <td>Diketahui Oleh</td>
                        </tr>
                        <tr>
                            <td style="weight:300px; height:130px">
                                <img src="/imageFile/signature/{{$detailReporting->operator_sign_check_in}}" alt="" width="200" height="80">
                            </td>
                            <td>
                                <img style="weight:300px; height:130px"src="/imageFile/signature/{{$detailReporting->driver_sign_check_in}}" alt="" width="200" height="80">
                            </td>
                        </tr>
                        <tr>
                            <td>Security: {{$detailReporting->operator}}<br>Tgl: {{$detailReporting->operator_sign_check_in_datetime}}</td>
                            <td>Pengemudi: {{$detailReporting->name}}<br>Tgl: {{$detailReporting->driver_sign_check_in_in_datetime}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if($detailReporting->is_checkout==1) 
    <div>
        <p></p>
        <p></p>    
        <p></p>    
        <h4>SEBELUM KELUAR SITE</h4>
        <table class="table table-bordered"  width="70%" cellspacing="0">
            <thead>
                    <tr>
                        <th>No</th>
                        <th>Aktifitas</th>
                        <th>Ya</th>
                        <th>Tidak</th>
                        <th>N/A</th>
                        <th>Keterangan</th>
                    </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($getAnswerCheckOuts as $getAnswerCheckOut)
                <tr>   
                    <td>{{ $i }}</td>
                    <td>{{$getAnswerCheckOut->question}}</td>
                    <td>
                        @if($getAnswerCheckOut->approve_check_out==1) 
                            <center>
                                <input type="radio" checked disabled>
                            </center>
                        @elseif($getAnswerCheckOut->approve_check_out==0) 
                            <center>
                                <input type="radio" disabled>
                            </center>
                        @endif
                    </td>
                    <td>
                        @if($getAnswerCheckOut->decline_check_out==1) 
                            <center>
                                <input type="radio" checked disabled>
                            </center>
                        @elseif($getAnswerCheckOut->decline_check_out==0) 
                            <center>
                                <input type="radio" disabled>
                            </center>
                        @endif
                    </td>
                    <td>
                        @if($getAnswerCheckOut->na_check_out==1) 
                            <center>
                                <input type="radio" checked disabled>
                            </center>
                        @elseif($getAnswerCheckOut->na_check_out==0) 
                            <center>
                                <input type="radio" disabled>
                            </center>
                        @endif
                    </td>
                    <td>{{$getAnswerCheckOut->annotation_check_out}}</td>
                    @php
                        $i = $i + 1;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
        <p></p>
        <p></p>
        <div class="row">
            <div class="col-md-8">
                <p>Keterangan : N/A = Tidak berlaku</p>
            </div>
            <div class="col-md-4">
                <table style="float: left;" class="tblsign table-bordered table-striped stacktable large-only" width="70%" cellspacing="0" cellpadding="2" border="1">
                    <tbody>
                        <tr>
                            <td>Diperiksa Oleh</td>
                            <td>Diketahui Oleh</td>
                        </tr>
                        <tr>
                            <td style="weight:300px; height:130px">
                                <img src="/imageFile/signature/{{$detailReporting->operator_sign_check_out}}" alt="" width="200" height="80">
                            </td>
                            <td>
                                <img style="weight:300px; height:130px"src="/imageFile/signature/{{$detailReporting->driver_sign_check_out}}" alt="" width="200" height="80">
                            </td>
                        </tr>
                        <tr>
                            <td>Security: {{$detailReporting->operator}}<br>Tgl: {{$detailReporting->operator_sign_check_out_datetime}}</td>
                            <td>Pengemudi: {{$detailReporting->name}}<br>Tgl: {{$detailReporting->driver_sign_check_out_datetime}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
</div>
<p></p>
<p></p>

<!-- <button type="submit" class="btn btn-info btn-flat" onclick="close()"> Close</button> -->

@endsection
@section('js')
<script>
function close(){
    console.log("test")
    window.location.href = "reporting";
}

</script>    

@endsection