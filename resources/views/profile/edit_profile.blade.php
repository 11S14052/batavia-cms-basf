@extends('master')

@section('content')
    <h4>Nomor Induk Karyawan : {{ $profile->id_employee }}</h4>
    <form action="/save-update-profile" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-8">
                <input type="hidden" name="id_employee" id="id_employee" value="{{ $profile->id_employee}}">
                <div>
                    <div class="row">
                        <label for="name" class="col-3">Full Name</label>
                        {{": "}}&nbsp;
                        <input type="text" id="name" name="name" value="{{ $profile->name}}" class="border-left-0 border-top-0 border-right-0 col-5 pl-0 text-capitalize" required>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="email" class="col-3">E-mail Address</label>
                        {{": "}}&nbsp;
                        <input type="email" id="email" name="email" value="{{ $profile->email}}" class="border-left-0 border-top-0 border-right-0 col-5 pl-0" required>
                    </div>
                </div>
                <div>
                    <div class="row">
                    <label for="phone" class="col-3">Phone Number</label>
                    {{": "}}&nbsp;
                    <input type="number" id="phone" name="phone" value="{{ $profile->phone}}" class="border-left-0 border-top-0 border-right-0 col-5 pl-0" required>
                </div>
                <div>
                    <div class="row">
                        <label for="birth_date" class="col-3">Date of Birth</label>
                        {{": "}}&nbsp;
                        <input type="date" id="birth_date"  name="birth_date" value="{{ $profile->birth_date}}" class="border-left-0 border-top-0 border-right-0 col-5 pl-0">
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="gender" class="col-3">Jenis Kelamin</label>
                        {{": "}}&nbsp;
                        <select name="gender" id="gender" value="{{ $profile->gender}}" class="border-left-0 border-top-0 border-right-0 col-5" required>
                            @if($profile->gender == "Laki-laki")
                                <option value="-- Pilih Jenis Kelamin --" disabled>-- Pilih Jenis Kelamin --</option>
                                <option value="Laki-laki" selected>Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            @elseif($profile->gender == "Perempuan")
                                <option value="-- Pilih Jenis Kelamin --" disabled>-- Pilih Jenis Kelamin --</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan" selected>Perempuan</option>
                            @else
                                <option value="-- Pilih Jenis Kelamin --" selected disabled>-- Pilih Jenis Kelamin --</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="position" class="col-3">Position</label>
                        {{": "}}&nbsp;
                        <input type="text" id="position" name="position" value="{{ $profile->position}}" class="border-left-0 border-top-0 border-right-0 col-5 pl-0" required>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="privilege" class="col-3">Previllage</label>
                        {{": "}}&nbsp;
                        <select name="privilege" id="privilege" class="border-left-0 border-top-0 border-right-0 col-5" required>
                            @foreach ($dataPrivileges as $index => $dataPrivilege)
                                <option value="{{$dataPrivilege->id}}">{{$dataPrivilege->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <label for="status" class="col-3">Status</label>
                        {{": "}}&nbsp;
                        <select name="status" id="status" class="border-left-0 border-top-0 border-right-0 col-5" required>
                            @if ($profile->is_active != null)
                            <option value="1" selected>Aktif</option>
                            <option value="0">Tidak Aktif</option>
                            @else
                            <option value="1">Aktif</option>
                            <option value="0" selected>Tidak Aktif</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
                <label for="image">Photo Profile</label><br/>
                @if ($profile->image =="")
                    <img style="border:5px double black;" src="/imageFile/imageProfile/avatar.png" id="profile-img-tag" width="180" height="190"/> 
                @else
                    <img style="border:5px double black;" src="/imageFile/imageProfile/{{$profile->image}}" id="profile-img-tag" width="180" height="190"/> 
                @endif
                <input onchange="previewFile(this)" class=""type="file" name="image" id="image">
                <div>{{ $errors->first('image') }}</div>
        </div>
        <div class="col-5 mt-4 mx-auto">
            <button class="btn btn-primary btn-block" type="submit">Save Change</button>
        </div>
    </form>
   
@endsection

@section('js')
    <script>
        function previewFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
            reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection

