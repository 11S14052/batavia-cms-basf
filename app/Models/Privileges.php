<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privileges extends Model
{
    protected $table = 'privileges';
    public $primaryKey = 'id';
}
