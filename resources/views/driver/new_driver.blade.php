@extends('master')

@section('content')
    <form action="" type="POST">
        <div>
            <label for="nama">Nama Pengemudi</label>
            <input type="text" id="nama" name="nama" required>
        </div>
        <div>
            <label for="jenis_sim">Jenis Sim</label>
            <select name="jenis_sim" id="jenis_sim" required>
                <option value="a">Sim A</option>
                <option value="b1">Sim B I</option>
                <option value="b2">Sim B II</option>
            </select>
        </div>
        <div>
            <label for="no_sim">Nomor SIM</label>
            <input type="text" id="no_sim" name="no_sim">
        </div>
        <div>
            <label for="sim_expiry">Kadaluarsa Sim</label>
            <input type="date" id="sim_expiry" name="sim_expiry">
        </div>
        <div>
            <button type="submit">Tambah</button>
        </div>
@endsection