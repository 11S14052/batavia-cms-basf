<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Models\Trucks;
use App\Models\Transportertypes;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class TruckController extends Controller
{
    public function daftartruck(){
        $truck = Trucks::all();
        if ($truck) {
            return view('truck.truck_list')->with('datatruck', $truck);
        }
    }

    public function editTruck(Request $request, $truck){
        $detailTruck = Trucks::find($truck)->first();
        if ($truck){
            return view('truck.ubah_truck')->with('truck', $detailTruck);
        }
    }

    public function saveEditTruck(){
        $id = request('id');
        $truck = Trucks::find($id);
        $truck->truck_name = request('truck_name');
        $truck->is_active = request('status');
        $truck->company = request('company');
        $truck->save();
        return redirect('/truck')->with('message', 'Berhasil Diubah!');
    }

    public function saveCreateTruck(){
        $truck = new Trucks;
        $truck->truck_name = request('truck_name');
        $truck->is_active = request('status');
        $truck->company = request('company');
        $truck->save();
        return redirect('/truck')->with('message', 'Berhasil Ditambahkan!');
    }
    public function deleteTruck(Request $request, $truck_id){
        // if (session('role')=='Admin') {
            $getTruck = Trucks::find($truck_id)->first();
            $getTruck->delete();
            return redirect('/truck')->with('message', 'Berhasil Dihapus!');
        // } else {
            // return redirect('/restaurant_admin')->with('message', 'Anda Bukan Admin!');
        // }
    }
}
