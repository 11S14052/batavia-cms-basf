<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Privileges;
use Illuminate\Http\Request;

class PrivilegeController extends Controller
{
    public function privilegelist(){
        $privilege = Privileges::all();
        if ($privilege) {
            return view('privilege.privilege_list')->with('dataprivilege', $privilege);
        }
    }
    public function tambahprivilege(){
        $privilege = new Privileges;
        $privilege->name = request('privilege');
        $privilege->view_user = request('view_user');
        $privilege->add_user = request('add_user');
        $privilege->edit_user = request('edit_user');
        $privilege->delete_user = request('delete_user');
        $privilege->view_role = request('view_role');
        $privilege->add_role = request('add_role');
        $privilege->setting_role = request('setting_role');
        $privilege->delete_role = request('delete_role');
        $privilege->view_report = request('view_report');
        $privilege->detail_report = request('detail_report');
        $privilege->add_ncm = request('add_ncm');
        $privilege->edit_ncm = request('edit_ncm');
        $privilege->approve_report = request('approve_report');
        $privilege->cancel_approve = request('cancel_approve');
        $privilege->view_trash = request('view_trash');
        $privilege->restore_trash = request('restore_trash');
        $privilege->remove_permanent = request('remove_permanent');
        $privilege->save();
        return redirect('user/group')->with('message', 'Berhasil Ditambahkan!');
    }

    public function ubahprivilege(Request $request, $privilege_id){
        $privilege = Privileges::find($privilege_id);
        if ($privilege){
            return view('privilege.edit_privilege')->with('privilege', $privilege);
        }
    }
    public function hapus_privilege(Request $request, $privilege_id){
        $privilege = Privileges::find($privilege_id);
        $privilege->delete();
        return redirect('/user/group')->with('message', 'Berhasil Dihapus!');
    }

    public function ubahprivilegeprocess(){
        $privilege = Privileges::find(request('id'));
        $privilege->name = request('privilege');
        $privilege->view_user = request('view_user');
        $privilege->add_user = request('add_user');
        $privilege->edit_user = request('edit_user');
        $privilege->delete_user = request('delete_user');
        $privilege->view_role = request('view_role');
        $privilege->add_role = request('add_role');
        $privilege->setting_role = request('setting_role');
        $privilege->delete_role = request('delete_role');
        $privilege->view_report = request('view_report');
        $privilege->detail_report = request('detail_report');
        $privilege->add_ncm = request('add_ncm');
        $privilege->edit_ncm = request('edit_ncm');
        $privilege->approve_report = request('approve_report');
        $privilege->cancel_approve = request('cancel_approve');
        $privilege->view_trash = request('view_trash');
        $privilege->restore_trash = request('restore_trash');
        $privilege->remove_permanent = request('remove_permanent');
        $privilege->save();
        return redirect('user/group')->with('message', 'Berhasil Ditambahkan!');
    }
}
