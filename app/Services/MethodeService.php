<?php
namespace App\Services;
use Illuminate\Support\Facades\View;
use DateTime;

class MethodeService {
    // dipakai tapi masih bug
    public function decryptData($jsonString){
        $passphrase = "ls1KlnDpyl2ZJT0vdNX1tNygAftBlgah";
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        dd($data);

        return json_decode($data, true);
    }

    public function changeFormatDate($date){
        $month = "";
        $newDate = date_parse_from_format("d-m-Y", $date);
        if($newDate["month"] == 1){
            $month = "Januari";
        } else if($newDate["month"] == 2){
            $month = "Februari";
        } else if($newDate["month"] == 3){
            $month = "Maret";
        } else if($newDate["month"] == 4){
            $month = "April";
        } else if($newDate["month"] == 5){
            $month = "Mei";
        } else if($newDate["month"] == 6){
            $month = "Juni";
        } else if($newDate["month"] == 7){
            $month = "Juli";
        } else if($newDate["month"] == 8){
            $month = "Agustus";
        } else if($newDate["month"] == 9){
            $month = "September";
        } else if($newDate["month"] == 10){
            $month = "Oktober";
        } else if($newDate["month"] == 11){
            $month = "November";
        } else if($newDate["month"] == 12){
            $month = "Desember";
        } 
        return $newDate["day"] ."-".$month."-".$newDate["year"];        
    }

    public function decryptCheckKeyAndReturnValue($valueEncrypt){
        dd($valueEncrypt);
        $valArray = explode('_', $valueEncrypt);
        $firstKey = $this->decryptKey($valArray[0]);
        $secondKey = $this->decryptKey($valArray[2]);

        if(($firstKey === env('URL_KEY')) && ($secondKey===env('URL_KEY'))){
            $newValue = "";
            for( $i = 0; $i < strlen($valArray[1]); $i++ ) {
                $b = ord($valArray[1][$i]);
                $a = $b ^ 123;  // <-- must be same number used to encode the character
                $newValue .= chr($a);
            }
            return intval($newValue) ;    
        } else {
            return null;
        }
    }

    public function decryptKey($valueDecrypt){
        $newValueKey = "";
        for( $i = 0; $i < strlen($valueDecrypt); $i++ ) {
            $b = ord($valueDecrypt[$i]);
            $a = $b ^ 10;  // <-- must be same number used to encode the character
            $newValueKey .= chr($a);
        }
        return $newValueKey;
    }

    public function downloadPdfFromURL($dest_path, $urlDocument, $userName){
        $newfname = $dest_path . "Loan Aggreement - ".$userName.".pdf";
        $file = fopen ($urlDocument, "rb");

        if ($file) {
            $newf = fopen ($newfname, "wb");

            if ($newf)
                while(!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
                }
        }

        if ($file) {
            fclose($file);
        }
        if ($newf) {
            fclose($newf);
        }
    }

}