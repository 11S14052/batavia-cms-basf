@extends('master')

@section('content')
    <h1> Nomor Dokumen : CGK-{{ $report->id }}</h1>
    <h1> NCM : 
        @if ($report->NCM)
            {{ $report->NCM }}
        @else
            Tidak Ada
        @endif
    </h1>
    <h1> Status : {{ $report->summary }}</h1>
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead class="thead-light">
            <th>Keterangan</th>
            <th>Isi</th>
        </thead>
        <tbody>
            <tr>
                <td>Nama Angkutan / Pelanggan / Supplier</td>
                <td>{{ $report->carrier_name }}</td>
            </tr>
            <tr>
                <td>Transporter Type</td>
                <td>{{ $report->transporter_type }}</td>
            </tr>
            <tr>
                <td>No Kendaraan</td>
                <td>{{ $report->vehicle_number }}</td>
            </tr>
            <tr>
                <td>No Tangki / Container</td>
                <td>{{ $report->tank_number }}</td>
            </tr>
            <tr>
                <td>No JO/DO</td>
                <td>{{ $report->DO_number }}</td>
            </tr>
            <tr>
                <td>Nama Driver</td>
                <td>{{ $report->driver_name }}</td>
            </tr>
            <tr>
                <td>Nomor Telepon Driver</td>
                <td>{{ $report->driver_phone }}</td>
            </tr>
            <tr>
                <td>Nama Produk</td>
                <td>{{ $report->product_name }}</td>
            </tr>
            <tr>
                <td>Check In Date</td>
                <td>{{ $report->created_at }}</td>
            </tr>
            <tr>
                <td>Check Out Date</td>
                <td>{{ $report->checkout_datetime }}</td>
            </tr>
            <tr>
                <td>Jenis SIM</td>
                <td>{{ $report->license_type }}</td>
            </tr>
            <tr>
                <td>No SIM</td>
                <td>{{ $report->license_number }}</td>
            </tr>
            <tr>
                <td>SIM berlaku sampai</td>
                <td>{{ $report->license_expiry_date }}</td>
            </tr>
            <tr>
                <td>STNK Berlaku Sampai</td>
                <td>{{ $report->vehicle_certificate_expiry_date }}</td>
            </tr>
            <tr>
                <td>KIR Berlaku Sampai</td>
                <td>{{ $report->kir_expiry_date }}</td>
            </tr>
            <tr>
                <td>Jenis Kendaraan</td>
                <td>{{ $report->vehicle_type }}</td>
            </tr>
        </tbody>
    </table>
    <div class="col-sm-8">
        <div class="my-1 mx-auto">
            <a href="/add_ncm/{{$report->id}}">
                <button class="btn btn-primary col-12">Add NCM</button>
            </a>
        </div>
        <div class="my-1 mx-auto" >
            <a href="/checkin_detail/{{$report->id}}">
                <button class="btn btn-primary col-12">Check In Detail</button>
            </a>
        </div>
        <div class="my-1 mx-auto">
            <a href="/checkout_detail/{{$report->id}}">
                <button class="btn btn-primary col-12">Check Out Detail</button>
            </a>
        </div>
        <div class="my-1 mx-auto">
            <a href="/accept_form/{{$report->id}}">
                <button class="btn btn-primary col-12">Accept</button>
            </a>
        </div>
        <div class="my-1 mx-auto">
            <a href="/decline_form/{{$report->id}}">
                <button class="btn btn-primary col-12">Decline</button>
            </a>
        </div>
    </div>
@endsection