@extends('master')

@section('content')
    <div class="card-header">
        <h6 class="card-title">ADD GROUP</h6>
    </div>
    <form action="tambahprivilageprocess" method="POST" enctype="multipart/form-data">
        @csrf
        <p></p>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-2">
                        <label for="privilage" class="font-weight-bold">Role Name</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" id="privilage" name="privilege" required>
                    </div>
                </div>
            </div>
        </div>
        <p></p>
        <h7 class="card-title"><b>ROLE LIST</b></h7>
        <p></p>
        <p></p>
        <div class="mb-3">
            <div class="row">
                <div class="col-xs-2 col-md-3 my-auto font-weight-bold">
                    <label for="">User Management</label>
                </div>
                <div class="col-xs-6 col-md-3">
                    <input type="checkbox" name="view_user" value="1"> View User<br/>
                    <input type="checkbox" name="add_user" value="1"> Add User<br/>
                    <input type="checkbox" name="edit_user" value="1"> Edit User<br/>
                    <input type="checkbox" name="delete_user" value="1"> Delete User<br/>
                </div>
            </div>
        </div>
        <hr/>
        <div class="mb-3">
            <div class="row">
                <div class="col-3 my-auto font-weight-bold">
                    <label for="">Role Management</label>
                </div>
                <div class="col-auto pl-0">
                    <input type="checkbox" name="view_role" value="1"> View Role <br/>
                    <input type="checkbox" name="add_role" value="1"> Add Role <br/>
                    <input type="checkbox" name="setting_role" value="1"> Setting Role <br/>
                    <input type="checkbox" name="delete_role" value="1"> Delete Role <br/>
                </div>
            </div>
        </div>
        <hr/>
        <div class="mb-3">
            <div class="row">
                <div class="col-3 my-auto font-weight-bold">
                    <label for="">Truck Report</label>
                </div>
                <div class="col-auto pl-0">
                    <input type="checkbox" name="view_report" value="1"> View Report<br/>
                    <input type="checkbox" name="detail_report" value="1"> Report Detail<br/>
                    <input type="checkbox" name="add_ncm" value="1"> Add NCM<br/>
                    <input type="checkbox" name="edit_ncm" value="1"> Edit NCM<br/>
                    <input type="checkbox" name="approve_report" value="1"> Approve<br/>
                    <input type="checkbox" name="cancel_approve" value="1"> Cancel Report<br/>
                </div>
            </div>
        </div>
        <hr/>
        <div class="mb-3">
            <div class="row">
                <div class="col-3 my-auto font-weight-bold">
                    <label for="">Trash Report</label>
                </div>
                <div class="col-auto pl-0">
                    <input type="checkbox" name="view_trash" value="1"> View Trash<br/>
                    <input type="checkbox" name="restore_trash" value="1"> Restore Trash<br/>
                    <input type="checkbox" name="remove_permanent" value="1"> Remove Permanent<br/>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-xs-0 col-sm-6">
            </div>
            <div class="col-sm-6">
                <button class="btn btn-primary btn-block" type="submit">Tambah</button>
            </div>
        </div>
    </form>
@endsection