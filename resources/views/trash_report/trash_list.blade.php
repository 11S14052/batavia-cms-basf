@extends('master')

@section('content')
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
            <tr>
                <th>No</th>
                <th>Jam Masuk/Keluar</th>
                <th>Nama Angkutan / Pelanggan / Supplier</th>
                <th>No.Kendaraan</th>
                <th>Nama Driver</th>
                <th>Jenis Kendaraan</th>
                <th>Code</th>
                <th>Status</th>
                <th>Kesimpulan</th>
                <th style="width: 150px">Action</th>
            </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($reportingTrucks as $truck)
        <tr>   
            <td>{{ $i }}</td>
            <td>{{ $truck->datetime_in_or_out }}</td>
            <td>{{ $truck->carrier_name}}</td>
            <td>{{ $truck->vehicle_number}}</td>
            <td>{{ $truck->name}}</td>
            <td>{{ $truck->vehicle_type}}</td>
            <td>{{ $truck->DO_number}}</td>
            <td>
                @if($truck->isOpen==1) 
                    <center>
                        <p style="color:green">Open</p>
                    </center>
                @elseif($truck->isOpen==0) 
                    <center>
                        <p style="color:red">Close</p>
                    </center>
                @endif
            </td>
            <td>
                @if($truck->isApprove==1) 
                    <center>
                        <span style="color:white" class="badge bg-success">Diterima</span>
                    </center>
                @elseif($truck->isApprove==0) 
                    <center>
                        <span style="color:white" class="badge bg-danger">Ditolak</span>
                    </center>
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i> Actions</button>
                    <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" onClick="restoreModal({{$truck->id}})"><i class="fa fa-eye"></i> Restore</a>
                        <a class="dropdown-item" onClick="removePermanentModal({{$truck->id}})"><i class="fa fa-times"></i> Remove Permanent</a>
                    </button>
                </div>
            </td>
            @php
                $i = $i + 1;
            @endphp
        </tr>
        @endforeach
    </tbody>
    <div class="modal fade" id="modalApproval" tabindex="-1" role="dialog" aria-labelledby="approveLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="approveLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="approve-bodyku">
          </div>
          <div class="modal-footer" id="approve-footerq">
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="rejectLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="rejectLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="reject-bodyku">
          </div>
          <div class="modal-footer" id="reject-footerq">
          </div>
        </div>
      </div>
    </div>
</table>
@endsection

@section('js')
<script>
function restoreModal(id){
    localStorage.setItem("idGeneralInform", id);
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Are you sure you want to restore this truck ?</label></div></form>';
    var title = 'Approve Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="restore(id)" type="button" class="btn btn-primary">Yes, restore</button>';
    setModalBoxApprove(title,content,footer,size);
    $('#modalApproval').modal('show');
 }
 function removePermanentModal(id){
    localStorage.setItem("idGeneralInform", id);
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Are you sure you want to remove permanent this truck ?</label></div> <div class="row"><div class="col-md-1"></div><div class="col-md-2"><label></div></form>';
    var title = 'Reject Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="removePermanent()" type="button" class="btn btn-primary">Yes, remove permanent</button>';
    setModalBoxReject(title,content,footer,size);
    $('#modalReject').modal('show');
 }

 function setModalBoxApprove(title,content,footer,$size){
    document.getElementById('approve-bodyku').innerHTML=content;
    document.getElementById('approveLabel').innerText=title;

    document.getElementById('approve-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#approveModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#approveModal').attr('class', 'modal fade')
            .attr('aria-labelledby','approveLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#approveModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

  function setModalBoxReject(title,content,footer,$size){
    document.getElementById('reject-bodyku').innerHTML=content;
    document.getElementById('rejectLabel').innerText=title;

    document.getElementById('reject-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#rejectModal').attr('class', 'modal fade')
            .attr('aria-labelledby','rejectLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

function restore(){
    const id = localStorage.getItem("idGeneralInform");
    localStorage.clear();
    window.location.href = '{{ env('APP_URL') }}' + '/trash/restore/'+id;
}
// cancel approve
function removePermanent(){
    const id = localStorage.getItem("idGeneralInform");
    localStorage.clear();
    window.location.href = '{{ env('APP_URL') }}' + '/trash/remove-permanent/'+id;
}
</script>    

@endsection