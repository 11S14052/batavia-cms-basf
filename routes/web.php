<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login
Route::get('/', function () {
    $nik = session('nik');
    if ($nik) {
        return redirect('/dashboard');
    }else{
        return redirect('/login');
    }
});
Route::get('/login', function () {
    return view('login.login_app');
});

//logout
Route::get('/logout', function () {
    Session::flush();
    return redirect('/');
});
Route::get('/form', function () {
    return view('form.new_form');
});
Route::get('/driver', function () {
    return view('driver.new_driver');
});

Route::get('/dashboard', 'AdminController@dashboard');


//login
Route::get('/login', function () {
    return view('login.login_app');
});
Route::post('/loginprocess', 'LoginController@loginprocess');
// Route::get('/login', 'LoginController@Login');


//dashboard
Route::get('/dashboard', function () {
    return view('dashboard.dashboard');
});

//user
Route::get('/user/management', 'UserController@daftaruser');
Route::get('/user/edit/{user_id}', 'UserController@editUser');
Route::post('/save-update-user-management', 'UserController@saveUpdateUserManagement');
Route::get('/hapus_user/{user_id}', 'UserController@hapus_user');
Route::get('/user/add', function () {
    return view('user.new_user');
});
Route::post('/tambahuserprocess', 'UserController@tambahuser');

//privilege
Route::get('/user/add-group', function () {
    return view('privilege.add_privilege');
});
Route::post('/user/tambahprivilageprocess', 'PrivilegeController@tambahprivilege');
Route::get('/user/group', 'PrivilegeController@privilegelist');
Route::get('/user/edit-group/{privilege_id}', 'PrivilegeController@ubahprivilege');
Route::post('/ubahprivilegeprocess', 'PrivilegeController@ubahprivilegeprocess');
Route::get('user/hapus_privilege/{privilege_id}', 'PrivilegeController@hapus_privilege');

//truck
Route::get('/truck', 'TruckController@daftartruck');
Route::get('/edit-truck/{truck_id}', 'TruckController@editTruck');
Route::post('/saveEditTruck', 'TruckController@saveEditTruck');
Route::get('/delete-truck/{truck_id}', 'TruckController@deleteTruck');
Route::get('/create-truck', function () {
    return view('truck.new_truck');
});
Route::post('/saveCreateTruck', 'TruckController@saveCreateTruck');

//Transporter Type
Route::get('/transporter-type', 'TransporterController@daftartransporter');
Route::get('/transporter-type/edit/{transporter_id}', 'TransporterController@edit');
Route::post('/transporter-type/edit/save-update', 'TransporterController@saveUpdate');
Route::get('/transporter-type/delete/{transporter_id}', 'TransporterController@delete');
Route::get('/transporter-type/create', function () {
    return view('transporter_type.new_type');
});
Route::post('/transporter-type/save-create', 'TransporterController@saveCreate');

//check in section
Route::get('/checkin-section/create', function () {
    return view('check_in_section.add_section');
});
Route::post('/checkin-section/save-create', 'CheckinSectionController@create');
Route::get('/checkin-section', 'CheckinSectionController@sectionlist');
Route::get('/checkin-section/edit/{section_id}', 'CheckinSectionController@edit');
Route::post('/checkin-section/edit/save-update', 'CheckinSectionController@saveUpdate');
Route::get('/checkin-section/delete/{section_id}', 'CheckinSectionController@delete');

//check out section
Route::get('/checkout-section/create', function () {
    return view('check_out_section.add_section');
});
Route::post('/checkout-section/save-create', 'CheckoutSectionController@create');
Route::get('/checkout-section', 'CheckoutSectionController@sectionlist');
Route::get('/checkout-section/edit/{section_id}', 'CheckoutSectionController@edit');
Route::post('/checkout-section/edit/save-update', 'CheckoutSectionController@saveUpdate');
Route::get('/checkout-section/delete/{section_id}', 'CheckoutSectionController@delete');

//check in statement
Route::get('/checkin-statement/create', function () {
    return view('check_in_statement.add_statement');
});
Route::post('/save-create', 'CheckinStatementController@saveCreate');
Route::get('/checkin-statement', 'CheckinStatementController@statementlist');
Route::get('/checkin-statement/edit/{statement_id}', 'CheckinStatementController@edit');
Route::post('/save-update', 'CheckinStatementController@saveUpdate');
Route::get('/checkin-statement/delete/{statement_id}', 'CheckinStatementController@delete');

//check out statement
Route::get('/checkout-statement/create', function () {
    return view('check_out_statement.add_statement');
});
Route::post('/checkout-statement/save-create', 'CheckoutStatementController@create');
Route::get('/checkout-statement', 'CheckoutStatementController@statementlist');
Route::get('/checkout-statement/edit/{statement_id}', 'CheckoutStatementController@edit');
Route::post('/checkout-statement/edit/save-update', 'CheckoutStatementController@saveUpdate');
Route::get('/checkout-statement/delete/{statement_id}', 'CheckoutStatementController@delete');

//profile
Route::get('/profile', 'ProfileController@viewProfile');
Route::get('/edit-profile', 'ProfileController@editProfile');
Route::post('/save-update-profile', 'ProfileController@saveUpdateProfile');
Route::get('/change-password', function () {
    return view('profile.change_password');
});
Route::post('/saveUpdatePassword', 'ProfileController@saveUpdatePassword');

//report
Route::get('/report', 'ReportController@daftarreport');
Route::get('/reportdetail/{report_id}', 'ReportController@reportdetail');
Route::get('/trash_report/{report_id}', 'ReportController@trashreport');
Route::get('/checkin_detail/{report_id}', 'ReportController@checkindetail');
Route::get('/checkout_detail/{report_id}', 'ReportController@checkoutdetail');
Route::get('/accept_form/{report_id}', 'ReportController@acceptreport');
Route::get('/decline_form/{report_id}', 'ReportController@declinereport');

//trash
Route::get('/trash', 'TrashController@daftartrash');
Route::get('/trash/restore/{report_id}', 'TrashController@restore');
Route::get('/trash/remove-permanent/{report_id}', 'TrashController@removePermanent');

//NCM
Route::post('/tambahncmprocess', 'ReportController@addncmprocess');
Route::get('/add_ncm/{report_id}', 'ReportController@addncm');

// egatelist 
Route::get('/reporting', 'EGateListController@index');
Route::get('/reporting/detail/{report_id}', 'EGateListController@reportDetail');

Route::get('/approval-trucks', 'ApprovalTruckController@index');
Route::get('/approval-truck/detail/{truck_id}', 'ApprovalTruckController@detail');
Route::get('/approval-truck/approve/{truck_id}', 'ApprovalTruckController@approveTruck');
Route::get('/approval-truck/rejected', 'ApprovalTruckController@rejectTruck');
Route::get('/reporting/cancel-reject/{truck_id}', 'ApprovalTruckController@cancelReject');
Route::get('/reporting/cancel-approve', 'ApprovalTruckController@cancelApprove');
