@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="box-title">TRANSPORTER TYPE - Edit </h5>
    </div>
</div>
<hr/>

<form action="save-update" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-2">
            <label for="type">Transporter Type</label>
        </div>
        <div class="col-md-5">
            <input type="text" id="type" name="type" required value="{{$transporter->type}}">
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-md-2">
            <label for="desc">Description</label>
        </div>
        <div class="col-md-5">
            <input type="text" id="desc" name="desc" required value="{{$transporter->description}}">
        </div>
    </div>
    <hr/>
    <div>
        <button class="btn btn-primary" type="submit">Update</button>
    </div>
    <input type="hidden" name="id" id="id" value="{{$transporter->id}}">
</form>
    
@endsection