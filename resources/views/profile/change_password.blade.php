@extends('master')

@section('content')
    @if (!empty($massage))
    <script>
        alert("{{$message}}");
    </script>
    @endif
    <form action="/saveUpdatePassword" method="POST" enctype="multipart/form-data">
        @csrf
        <div>
            <div class="row">
                <label for="oldpass" class="col-3">Old Password</label>
                <input type="password" id="oldpass" name="oldpass" class="border-left-0 border-top-0 border-right-0 col-3" maxLength="20" required>
            </div>
        </div>
        <div>
            <div class="row">
                <label for="newPassword" class="col-3">New Password</label>
                <input type="password" id="newPassword" name="newPassword" class="border-left-0 border-top-0 border-right-0 col-3" maxLength="20" required>
            </div>
        </div>
        <div>
            <div class="row">
                <label for="confirmNewPassword" class="col-3">Verify New Password</label>
                <input type="password" id="confirmNewPassword" name="confirmNewPassword" class="border-left-0 border-top-0 border-right-0 col-3" maxLength="20" required>
            </div>
        </div>
        
        <div class="col-4 pl-0 mt-4 mx-auto">
            <button class="btn btn-primary btn-block" type="submit">Save</button>
        </div>
    </form>
   
@endsection