@extends('master')

@section('content')
<div class="box box-primary">
    <div class="row">
        <div class="col-md-9">
            <div class="box-header with-border">
                <h4 class="box-title">Tambah Transporter yang disewa BASF </h4>
            </div>
        </div>
        <!-- <div class="col-md-1">
            <div class="box-header with-border">
                <button class="btn btn-block btn-danger" type="submit">Back</button>
            </div>
        </div> -->
    </div>
    <hr/>
    <form action="/saveCreateTruck" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <label for="truck_name">Nama Kendaraan *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control col-md-7" type="text" id="truck_name" name="truck_name" required>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-2">
                        <label for="company">Nama Perusahaan *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control col-md-7" type="text" id="company" name="company" required>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-2">
                        <label for="status">Status</label>
                    </div>
                    <div class="col-md-9">
                        <select id="status" name="status" required>
                            <option selected disabled >-- Pilih Status --</option>
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                    </div>
                </div>  
                <hr/>
                <div>
                    <button class="btn btn-info" type="submit">Save</button>
                </div>
            </div>
    </form>
</div>
@endsection