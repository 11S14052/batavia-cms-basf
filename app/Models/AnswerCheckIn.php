<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerCheckIn extends Model
{
    protected $table = 'answer_check_in';
}
